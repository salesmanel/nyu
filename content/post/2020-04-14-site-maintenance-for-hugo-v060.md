---
title: Site Maintenance for Hugo v0.60
subtitle: Site Maintenance
comments: false
date: 2020-04-14
categories: ["blogging"]
tags: ["blogging", "casa"]
#draft: false
---

### Background

I lacked motivation to keep [this tweaked theme][mybh] up with [Hugo]'s
development after last summer.  When I came back yesterday, I saw that some
pages using [Font Awesome][fa] icons were broken.  Apart from that, the
[ToC list level problem][tocpblm] resurfaced.

### Fixed ToC

The [solution that I had adopted][pastsoln] no longer worked.  Luckily, some
users provided a shorter code in the later discussions.  The
[one from user501254][soln0] caught me.  I first tried to copy his code into the
partial layout for ToC.  However, nothing changed.

```go-html-template
{{ $emtLiPtrn := "(?s)<ul>\\s<li>\\s<ul>(.*)</li>\\s</ul>" }}
{{ $rplcEmtLi := "<ul>$1" }}
{{ .TableOfContents | replaceRE $emtLiPtrn $rplcEmtLi | safeHTML }}
```

In an attempt to identify the cause of this error, I looked for the official
docs for [`replaceRE`][replaceRE], whose syntax seemed to be unchanged.  I then
played with the `PATTERN` and `REPLACEMENT` with some simple syntax---that
worked.  This showed that the problem was about `$emtLiPtrn`.  Thanks to
https://regex101.com/ and the generated HTML source code, I found that appending
an asterisk `*` after each `\\s` in `$emtLiPtrn` fixed the issue.

```html
<aside class="toc-article">
<nav id="TableOfContents">
  <ul>
    <li>
      <ul>
        <li><a href="#for-each-ref">for-each-ref</a></li>
        <li><a href="#ls-files-ls-tree">ls-files, ls-tree</a></li>
        <li><a href="#merge-base">merge-base</a></li>
        <li><a href="#merge---squash-rebase--i">merge –squash, rebase -i</a></li>
        <li><a href="#reflog">reflog</a></li>
        <li><a href="#rev-parse">rev-parse</a></li>
        <li><a href="#diff">diff</a></li>
      </ul>
    </li>
  </ul>
</nav>
</aside>
```

This fix is available online at commit [a3d7343a] on GitLab.

Remarks: In the linked regex testing website, the "golang" favour has to be
chosen, and use `\s` instead of `\\s`.

### Fixed missing Font Awesome icons

In the [page for Math.SE comment templates][mathsecmt], the text areas
disappeared.  Inspecting the elements, the message `<!-- raw HTML omitted -->`
popped up.  Thanks to [this Japanese article][soln], I quickly solved this
problem at commit [74c81df0] on GitLab.

[mybh]: https://gitlab.com/VincentTam/beautifulhugo
[Hugo]: https;//gohugo.io
[fa]: https://fontawesome.com
[tocpblm]: /post/2018-09-17-fix-hugo-table-of-contents/
[pastsoln]: /post/2018-09-27-better-hugo-toc-fix/
[soln0]: https://github.com/gohugoio/hugo/issues/1778#issuecomment-483880269
[replaceRE]: https://gohugo.io/functions/replacere/#readout
[a3d7343a]: https://gitlab.com/VincentTam/beautifulhugo/-/commit/a3d7343a255fbbb277be0c6e37eba2b65bcd2477
[mathsecmt]: /page/math-se-comment-templates/
[soln]: https://budougumi0617.github.io/2020/03/10/hugo-render-raw-html/
[74c81df0]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/-/commit/74c81df0a9094002ca2e24e88e93484f6279910f
